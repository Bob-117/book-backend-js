const express = require('express'); // NodeJS Server Framework
const conf = require('./conf/server_config'); // x.x.x.x:XYZ
const moment = require('moment') // date
const mongo = require('./database/database') // BDD mongo

// main component
const server = express();

// request body read config
const bodyPost = require('body-parser');
server.use(bodyPost.json());
server.use(bodyPost.urlencoded({extended: false}));
server.use((req, res, next) => {
    res.header('Content-Type', 'application/json');
    next();
})

// Avoid Cors Issue
const allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*"); //allow only one ip / given ips
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}
server.use(allowCrossDomain);


// curl http://ip:port/
server.get('/', (req, res) => {
    res.status(200);
    let json_response = {
        in: "Hello there ! ",
        out: "General Kenobi"
    }
    res.json(json_response)
})

// get all books
// curl http://127.0.0.117:8080/book
server.get('/book', (req, res) => {
    // console.log('GET ALL');
    mongo.db.collection('book').find({}).toArray()
        .then(books => {
            if (!books) {
                res.status(204) // no content
                return res.send([]) // return empty array
            }
            res.status(200)
            //console.log(books)
            res.send(books || [])
        })
        .catch(err => console.log('An error occurred getting mongo todo list', err))
})

server.get('/author', (req, res) => {
    // console.log('GET ALL');
    mongo.db.collection('book').distinct('author')
        .then(author => {
            if (!author) {
                res.status(204) // no content
                return res.send([]) // return empty array
            }
            res.status(200)
            //console.log(books)
            res.send(author || [])
        })
        .catch(err => console.log('An error occurred getting mongo todo list', err))
})

// db.collection.distinct('x')

// post one book
// curl --X POST -H "Content-type:application/json" --data-binary "{\"title\":\"title\", \"author\":\"author\"}" http://127.0.0.117:8080/book
server.post('/book', (req, res) => {
    console.log('POST')
    const data = req.body
    let new_title = data.title || "default_title";
    let new_author = data.author || "default_author";
    let new_summary = data.summary || "lorem ipsum";
    let new_img = "false";
    let new_date = data.date || "default_date";
    let new_rating = data.rating || 5;
    let new_tag = data.tag || "default_tag";
    console.log(data)
    // creation du nouvel objet tache
    let newBook = {
        "title": new_title,
        "author": new_author,
        "summary": new_summary,
        "img": new_img,
        "date": new_date,
        "rating": new_rating,
        "tag": new_tag
    };
    mongo.db.collection('book').findOne({title: new_title})
        .then(result => {
            if (result) {
                console.log("Warning, ", data.title, " already exists!");
                res.status(403).end();
                return result;
            }
            // mongo.db.collection('book').insertOne(data);
            // db.books.insertOne({title: "titre", author: "auteur", summary: "resume", img: "/img/img1.png", date: "07-06-2022", rating: "5", tag: "default"})

            mongo.db.collection('book').insertOne(newBook, function (err, docsInserted) {
                console.log(docsInserted.insertedId);
                // res.send(newBook)
            });

            console.log('Todo successfully added to mongo database.');
            res.status(201); //created
            return res.send(JSON.stringify(data));
        })
        .catch(err => {
            console.log('An error occured inserting todo in mongo.', err)
        })
});

// delete one book
// curl -i -X DELETE http://127.0.0.117:8080/book -H "Content-type:application/json" --data-binary "{\"id_to_delete\":\"62aa18e5410063dbf877b382\"}"
server.delete('/book', (req, res) => {
    // console.log("DELETE")
    const id_to_delete = (req.body.id_to_delete);
    mongo.db.collection('book').findOne({_id: mongo.ObjectId(id_to_delete)})
        .then(book => {
            if (!book) {
                console.log("Warning, nothing to delete!");
                res.status(403).end();
                return book;
            }
            let itemToDelete = book;
            mongo.db.collection('book').deleteOne(itemToDelete);
            // findOne ? true : false
            console.log('Todo successfully deleted from mongo database.');
            res.status(202);
            // return res.send({"action": "delete", "target": '"' + itemToDelete + '"'});
            return res.send({"action": "delete", "target": '"' + id_to_delete + '"'})
        })
});

// update one book
// curl -X PUT -H "Content-Type: application/json" -d "{\"id_to_update\":\"62aa15c833bc7edefb5c738c\", \"updated_title\":\"update\"}" http://127.0.0.117:8080/book
server.put('/book', (req, res) => {
    // console.log("PUT")
    let item_to_edit = req.body
    // console.log(item_to_edit)
    return mongo.db.collection('book').findOne(
        {
            $or: [
                {_id: mongo.ObjectId(item_to_edit.id_to_update)},
                { title: item_to_edit.updated_title} //cant update existing title
            ]
        }
    )
        .then(result => {
            console.log(result)
            if (!result) {
                console.log("Warning, task to edit not found !");
                return res.status(403).end();
            }
            // let data = req.body;
            // let updatedTitle = data.title || result.title;
            // let updatedAuthor = data.author || result.author;
            return mongo.db.collection('book').updateOne(
                {_id: mongo.ObjectId(item_to_edit.id_to_update)},
                {
                    $set:
                        {
                            "title": item_to_edit.updated_title,
                            "author": item_to_edit.updated_author
                        }
                }
            )
        })
        .then(() => {
            return mongo.db.collection('book').findOne({_id: mongo.ObjectId(item_to_edit.id_to_update)})
        })
        .then(updated => {
            // console.log(" UPDATED :", updated);
            res.send(updated)
        })
        .catch(err => {
            console.log('An error occured editing todo in mongo.', err)
        })
});


// /book?sorted=true
// get all books sorted by author
server.get('/sorted_book', (req, res) => {
    console.log('GET SORTED');
    mongo.db.collection('book')
        .aggregate([
            { $group : { _id : "$author", books: { $push: "$title" } } }
        ]).toArray()
        .then(books => {
            if (!books) {
                res.status(200)
                return res.send([]) // return empty array
            }
            res.status(200)
            //console.log(books)
            res.send(books || [])
        })
        .catch(err => console.log('An error occurred getting mongo todo list', err))
})

server.get('/rated_book', (req, res) => {
    console.log('GET RATED');
    mongo.db.collection('book')
        .aggregate([
            { $group : { _id : "$author", books: { $push: "$title", $push: "$rating" } } }
        ]).toArray()
        .then(books => {
            if (!books) {
                res.status(200)
                return res.send([]) // return empty array
            }
            res.status(200)
            //console.log(books)
            res.send(books || [])
        })
        .catch(err => console.log('An error occurred getting mongo todo list', err))
})

// server.get('/book/:id', (req, res) => {
//     console.log("GET ONE")
//     const search_id = req.params.id;
//     mongo.db.collection('book').find({}).toArray()
//         .then(books => {
//             let book = books.find(element => {
//                 console.log(mongo.ObjectId(search_id))
//                 return((parseInt(element._id)).equals(mongo.ObjectId(search_id)));
//             });
//             res.status(200);
//             console.log('Resultat de la cherche avec l\'id : ' + search_id)
//             console.log(todoToDisplay_ById || 'Aucun resultat pour cette recherche');
//             console.log('------------------------')
//             res.send(book); // pas besoin de end() avec send() et stringify transforme ton json en string
//         })
//         .catch(err => console.log('An error occured searching todo with id', err));
// });

// // get all books from one single author
// // curl http://ip:port/book/julesverne
// server.get('/book/:author', (req, res) => {
//     console.log('------------------------')
//     console.log('/book/:author')
//     const author_filter = req.params.name;
//     mongo.db.collection('book').find({}).toArray()
//         .then(books => {
//             let result_book = books.find(element => {
//                 return(element.author === author_filter);
//             });
//             res.status(200);
//             console.log('------------------------')
//             console.log('Resultat de la cherche avec le mot : ' + author_filter)
//             console.log(result_book || 'Aucun resultat pour cette recherche');
//             console.log('------------------------')
//             res.send(result_book); // pas besoin de end() avec send()
//
//         })
//         .catch(err => console.log('An error occurred searching todo with name', err));
// });

mongo.connect('mongodb://127.0.0.1:27017', {useNewUrlParser: true})
    .then(() => {
        console.log('Connected to mongodb, we can now use mongo.db object.')
        server.listen(conf.port, conf.hostname, (err) => {
            if (err) {
                return console.log("Error:", err)
            }
            console.log('Server running at http://' + conf.hostname + ':' + conf.port + '/');
            console.log('today : ' + moment().format('DD-MM-YYYY hh:mm'))
        })

    })