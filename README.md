# NODE JS API REST - BackEnd server

### Uses express
### Uses mongodb

```shell
npm run start
```

## Front app with VueJS at https://gitlab.com/Bob-117/book-frontend-js
Le sujet : Création d'une application qui affiche une liste de livres (avec image, titre, auteur, résumé, date de création, notation)

Les composants attendus sont les suivants :

Une application « front » en VueJS
Un application « back » en NodeJS (Express, NestJS, …)
Une base de données Mongo
Une présentation de 10-15 min avec :

Une démonstration
L’explication des choix technologiques et architecture mise en place
Une présentation du code
